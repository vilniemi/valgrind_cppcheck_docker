# valgrind and cpp check on linux container

usage:
Copy your project to valgrind, then
    docker-compose build
    docker-compose up
Now your container should be up if it displays
"Attaching to valgrind"
and you can:
    ./access_valgrind
    
    build your app there
    ./valgrind_check_all.sh EXECUTABLE

    for static analysis run
    ./cpp_check_all.sh
